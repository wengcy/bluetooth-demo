import Vue from 'vue'
import App from './App'
import plugins from './static/utils/plugins.js';
Vue.use(plugins);
//#ifdef H5
 //require('./static/mock/index.js');
//#endif

Vue.config.productionTip = false

App.mpType = 'app'
import './static/css/common.scss'
import './static/fonts/iconfont.css'



const app = new Vue({
    ...App
})
app.$mount()
export default app;

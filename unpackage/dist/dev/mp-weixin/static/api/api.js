let requestUtil = require('./request.js');

//登录
export const login = (params)=>{
	return requestUtil("3001",params,"登陆中");
}

//表具查询
export const queryMeterInfo = (params)=>{
	return requestUtil("3002",params,"查询中");
}

//表具更换
export const changeMeter = (params)=>{
	return requestUtil("3003",params,"请求中");
}

//计算费用
export const computedFee = (params)=>{
	return requestUtil("3004",params,"正在计算");
}

//检查应答报文
export const validCommand = (params)=>{
	return requestUtil("2000",params,"");
}

//读卡类型
export const getCardTypeApi = (params)=>{
	return requestUtil("2001",params,"");
}

//读卡数据
export const getCardDataApi = (params)=>{
	return requestUtil("2002",params,"");
}

//写卡数据
export const writeCardDataApi = (params)=>{
	return requestUtil("2003",params,"");
}




let plugins = {
 	install(Vue) {
 		//模糊字符串 start 开始, end 倒数几个
 		Vue.prototype.blurStr = function(str, start, end, num = 3) {
 			if (typeof str == "undefined") return;
 			let count = "";
 			for (let i = 0; i < num; i++) {
 				count += "*"
 			}
 			return str.substring(0, start) + count + str.substring(str.length - end);
 		}

 		//根据对象值，返回对象
 		Vue.prototype.findObj = function(objArr, queryProp, value) {
 			let result = {};
 			objArr.forEach((item) => {
 				if (item[queryProp] == value) {
 					result = item;
 				}
 			})
 			return result;
 		}

 		//moneyFlag，是否金额，viewType 显示类型，
 		Vue.prototype.showTag = function(moneyFlag, viewType) {
 			let obj = {}
 			if (moneyFlag == '1') {
 				obj = {
 					lable: '金额',
 					unit: '元'
 				}
 			} else {
 				if (viewType == 'water') {
 					obj = {
 						lable: '水量',
 						unit: '吨'
 					}
 				} else {
 					obj = {
 						lable: '气量',
 						unit: '方'
 					}
 				}
 			}
 			return obj;
 		}

 		Vue.filter('dateFormate', function(value) {
			if (typeof value == "undefined") return;
 			return value.substring(0, 4) + "-" + value.substring(4, 6) + "-" + value.substring(6, 8) + " " + value.substring(
 				8, 10) + ":" + value.substring(10, 12) + ":" + value.substring(12, 14)
 		})

 		Vue.filter('dFormate', function(value) {
			if (typeof value == "undefined") return;
 			return value.substring(0, 4) + "-" + value.substring(4, 6) + "-" + value.substring(6, 8);
 		})

 		Vue.filter('dFormate', function(value) {
 			return value.substring(0, 4) + "-" + value.substring(4, 6) + "-" + value.substring(6, 8);
 		})

 		Vue.prototype.throttle = function(fn, gapTime) {
 			if (gapTime == null || gapTime == undefined) {
 				gapTime = 1500
 			}
 			let _lastTime = null
 			// 返回新的函数
 			return function() {
 				let _nowTime = +new Date()
 				if (_nowTime - _lastTime > gapTime || !_lastTime) {
 					fn.apply(this, arguments) //将this和参数传给原函数
 					_lastTime = _nowTime
 				}
 			}
 		}

 		Vue.prototype.showType = function(viewType) {
 			let obj = {};
 			if (viewType == 'water') {
 				obj = {
 					lable: '水量',
 					unit: '吨'
 				}
 			} else {
 				obj = {
 					lable: '气量',
 					unit: '方'
 				}
 			}
 			return obj;
 		}

 		Vue.prototype.showTag = function(moneyFlag, viewType) {
 			let obj = {}
 			if (moneyFlag == '1') {
 				obj = {
 					lable: '金额',
 					unit: '元'
 				}
 			} else {
 				if (viewType == 'water') {
 					obj = {
 						lable: '水量',
 						unit: '吨'
 					}
 				} else {
 					obj = {
 						lable: '气量',
 						unit: '方'
 					}
 				}
 			}
 			return obj;
 		}
 	}
 }

 export default plugins

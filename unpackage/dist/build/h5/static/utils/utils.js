let config = require('./config.js');
let util = {
	//获取资源文件路径
	getResourceUrl(url) {
		let flag = url.startsWith("/");
		if (flag) {
			url = config.baseUrl + url;
		}
		return url;
	},
	//点击节流事件
	throttle(fn, gapTime) {
		if (gapTime == null || gapTime == undefined) {
			gapTime = 1500
		}
		let _lastTime = null
		// 返回新的函数
		return function() {
			let _nowTime = +new Date()
			if (_nowTime - _lastTime > gapTime || !_lastTime) {
				fn.apply(this, arguments) //将this和参数传给原函数
				_lastTime = _nowTime
			}
		}
	},
	showType(viewType) {
		let obj = {};
		if (viewType == 'water') {
			obj = {
				lable: '水量',
				unit: '吨'
			}
		} else {
			obj = {
				lable: '气量',
				unit: '方'
			}
		}
		return obj;
	},
	//moneyFlag，是否金额，viewType 显示类型，
	showTag(moneyFlag, viewType) {
		let obj = {}
		if (moneyFlag == '1') {
			obj = {
				lable: '金额',
				unit: '元'
			}
		} else {
			if (viewType == 'water') {
				obj = {
					lable: '水量',
					unit: '吨'
				}
			} else {
				obj = {
					lable: '气量',
					unit: '方'
				}
			}
		}
		return obj;
	},
	//获取当前时间
	getNowTime: function() {
		let date = new Date();
		this.year = date.getFullYear();
		this.month = date.getMonth() + 1;
		this.date = date.getDate();
		this.month = this.month < 10 ? "0" + this.month : this.month;
		this.date = this.date < 10 ? "0" + this.date : this.date;
		this.hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
		this.minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
		this.second = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
		let currentTime = this.year + this.month + this.date + this.hour + this.minute + this.second;
		return currentTime;
	},
	//获取格式化时间
	formateByTime: function(time) {
		if (typeof time == "undefined") return;
		let newTime = time.substring(0, 4) + "-" + time.substring(4, 6) + "-" + time.substring(6, 8) + " " + time.substring(
			8, 10) + ":" + time.substring(10, 12) + ":" + time.substring(12, 14);
		return newTime;
	},
	//获取格式化时间
	formateByDate: function(time) {
		if (typeof time == "undefined") return;
		let date = time.substring(0, 4) + "-" + time.substring(4, 6) + "-" + time.substring(6, 8);
		return date;
	},

	//showModal
	showModal: function(content, callback = function() {}, showCancel = false, confirmText = "确定") {
		uni.showModal({
			title: '提示',
			content: content,
			showCancel: showCancel,
			confirmText: confirmText,
			success(res) {
				if (res.confirm) {
					callback();
				}
			}
		});
	},
	//跳转
	reLaunch: function(url) {
		uni.reLaunch({
			url: url
		});
	},
	//跳转
	navigateTo: function(url) {
		uni.navigateTo({
			url: url
		});
	},
	//设置缓存数据
	setStorage: function(key, value) {
		uni.setStorage({
			key: key,
			data: value
		})
	},
	unique(arr) {
		return Array.from(new Set(arr)); // 利用Array.from将Set结构转换成数组
	},
	//去除重复的值
	setSplitNumber(list, prop) {
		let num = 5;
		let arr = [];
		for (let i = 0; i < list.length; i++) {
			arr.push(list[i][prop]);
		}
		let newArr = this.unique(arr);
		let len = newArr.length;
		if (len < 5) {
			num = len;
		}
		return num;
	},
	//显示loading
	showLoading: function(content) {
		uni.showLoading({
			title: content,
			mask: true
		})
	},
	//获取地址栏参数
	getQueryString: function(name) {
		var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
		var r = window.location.search.substr(1).match(reg);
		if (r != null) {
			return unescape(r[2]);
		}
		return null;
	},

	//格式化时间
	formatTime:function(param) {
		let y = param.getFullYear();
		let m = param.getMonth() + 1;
		let d = param.getDate();
		m = m < 10 ? ("0" + m) : m;
		d = d < 10 ? ("0" + d) : d;
		return y + "" + m + "" + d + "000000";
	},
	/**
	 * 以当前时间为基础，便捷获取时间（最近2天，最近1周，最近2周，最近1月，最近2月，最近半年，最近一年，本周，本月，本年）
	 * @param { string } code
	 * @returns { Object }
	 */
	getDate: function(code) {
		const date = new Date();
		let endTime = this.formatTime(date);
		let date1 = Date.parse(date);
		let start = '';
		let end = '';
		let oneDay = 1000 * 3600 * 24;

		switch (code) {
				//最近1月
			case 'lastOneMonth':
				start = new Date();
				start.setMonth(start.getMonth() - 1)
				break;
				//最近3月
			case 'lastThreeMonth':
				start = new Date();
				start.setMonth(start.getMonth() - 3)
				break;
				//最近半年
			case 'lastHalfYear':
				start = date1 - oneDay * 183;
				break;
				//最近一年
			case 'lastOneYear':
				start = new Date();
				start.setYear(start.getFullYear() - 1)
				break;
		}

		return {
			startTime: this.formatTime(new Date(start)),
			endTime: end ? this.formatTime(new Date(end)) : endTime,
		}
	}
}
module.exports = util;

let util = require('./utils.js');
let config = require('./config.js');
let api = {
	request({
		requestUrl = '',
		data = {},
		method = "get",
		isShowLoading = false,
		isShowContent = '请求中...',
		isReturnCode=false
	}) {
		return new Promise(async (resolve, reject) => {
			let that = this;
			data.reqTime = util.getNowTime();
			let params = { ...data,
				...config.requestDefaultParams,
			}

			if (isShowLoading) {
				util.showLoading(isShowContent);
			}
			let url = "/api";
			//#ifdef MP-WEIXIN 
			url = config.url,
				//#endif

				uni.request({
					url: url + requestUrl,
					method: method,
					data: params,
					timeout: 5000,
				}).then(response => {
					if (isShowLoading) {
						uni.hideLoading();
					}
					var [error, res] = response;
					data = res.data;
					if (error) {
						util.showModal("请求失败，请检查网络是否正常");
					} else {
						if (data.retCode == '0000') {
							if(isReturnCode) {
								resolve(data)
							}else{
								resolve(data.data);
							}
						} else if (data.retCode == '9990') {
							util.showModal('token已失效，返回首页', () => {
								util.reLaunch('/pages/index/index');
							});
						} else {
							util.showModal(data.retMsg);
							reject();
						}
					}
				})
		})
	},
	//获取配置中心参数
	getConfig() {
		let self = this;
		return new Promise(async (resolve, reject) => {
			let data = await self.request({
				requestUrl: "/userapi/config"
			});
			resolve(data);
		})
	},
	//获取用户信息
	getUserList() {
		let self = this;
		return new Promise(async (resolve, reject) => {
			let data = await self.request({
				requestUrl: "/userapi/userlist"
			});
			resolve(data);
		})
	},
	//获取用户信息
	getUserInfo(params) {
		let self = this;
		return new Promise(async (resolve, reject) => {
			let data = await self.request({
				requestUrl: "/userapi/qruser",
				data: params,
				isShowLoading: true,
				isShowContent: "正在校验"
			})
			resolve(data);
		})
	},
	//用户确认注册
	async sureRegister(params) {
		return new Promise(async (resolve, reject) => {
			 await this.request({
				requestUrl: "/userapi/reguser",
				data: params,
				isShowLoading: true,
				isShowContent: "正在注册",
				isReturnCode:true
			});
			resolve();
		})
	},
	//用户确认注册
	async updateUser(params) {
		return new Promise(async (resolve, reject) => {
			 await this.request({
				requestUrl: "/userapi/chgkind",
				data: params,
				isShowLoading: true,
				isShowContent: "正在修改",
				isReturnCode:true
			});
			resolve();
		})
	},
	//用户确认注册
	async unbind(params) {
		return new Promise(async (resolve, reject) => {
			 await this.request({
				requestUrl: "/userapi/deluser",
				data: params,
				isShowLoading: true,
				isShowContent: "正在解绑",
				isReturnCode:true
			});
			resolve();
		})
	},
	//2.2.6获取预付费记录
	async getpreuser(params) {
		return new Promise(async (resolve, reject) => {
			 let data = await this.request({
				requestUrl: "/userapi/getpreuser",
				data: params,
				isShowLoading: true,
			});
			resolve(data);
		})
	},
	//费用计算
	async getCountamt(params) {
		return new Promise(async (resolve, reject) => {
			 let data = await this.request({
				requestUrl: "/userapi/countamt",
				data: params,
				isShowLoading: true,
				isShowContent:"正在计算"
			});
			resolve(data);
		})
	},
	//2.2.8最近一年的交易明细
	async getOrderList(params) {
		return new Promise(async (resolve, reject) => {
			 let data = await this.request({
				requestUrl: "/userapi/orderlist",
				data: params,
				isShowLoading: true,
				isShowContent:"正在查询"
			});
			resolve(data);
		})
	},
	//调起支付
	async getPreorder(params) {
		return new Promise(async (resolve, reject) => {
			 let data = await this.request({
				requestUrl: "/orderapi/preorder",
				data: params,
				isShowLoading: true,
				isShowContent:"调起支付"
			});
			resolve(data);
		})
	},
	//3.2支付结果查询
	async getOrderstatus(params) {
		return new Promise(async (resolve, reject) => {
			 let data = await this.request({
				requestUrl: "/orderapi/orderstatus",
				data: params,
				isShowLoading: true,
				isShowContent:"支付结果查询"
			});
			resolve(data);
		})
	},
}

module.exports = api;

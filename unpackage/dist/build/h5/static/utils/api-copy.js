let util = require('./utils.js');
let config = require('./config.js');
let api = {
	request(requestUrl,data={},method="get", isShowLoading = false, isShowContent = '请求中...') {
		return new Promise(async (resolve, reject) => {
			let that = this;
			//data.reqTime = util.getNowTime();
			let defaultData = await util.getStorage('defaultData');
			let params = { ...data,
				...defaultData,
			}
			
			if (isShowLoading) {
				util.showLoading(isShowContent);
			}
			let url = "/api";
			//#ifdef MP-WEIXIN 
			    url = config.url,
			//#endif
			
			uni.request({
				url: url+requestUrl,
				method: method,
				data: params,
				timeout: 5000,
			}).then(data => {
				console.log(data[1])
				if (isShowLoading) {
					uni.hideLoading();
				}
				var [error, res] = data;
				if (error) {
					util.showModal("请求失败，请检查网络是否正常");
				} else {
					let data = res.data;
					if (res.statusCode == 200) {
						if (data.resultCode == '0000') {
							resolve(data);
						} else if(data.resultCode = '9990' == '9990'){
							util.showModal('token已失效，返回首页',()=>{
								util.reLaunch('/pages/index/index');
							});
						}else{
							util.showModal(data.resultMsg);
						}
					} else {
						util.showModal("请求失败，请检查网络")
					}
				}
			})
		})
	},
	
	//获取配置信息
	getSystemConfig() {
		let self = this;
		return new Promise(async (resolve, reject) => {
			let pdata = {
				reqCode: "8000",
			}
			let data = await self.request(pdata);
			util.setStorage('config', data);
			resolve(data);
		})
	},
	
	//获取伙伴编号和渠道编号
	getInitParams() {
		let params = {
			partner: util.getQueryString("partner"),
			channelNo: util.getQueryString("channelNo")
		}
		return params;
	},
	getCode() {
		return new Promise((resolve, reject) => {
			uni.login({
				success: function(res) {
					resolve(res.code);
				}
			});
		});
	},
	//获取配置信息
	getConfigInfo() {
		return new Promise(async (resolve, reject) => {
			let self = this;
			let params = {};
			let code = '';
			//#ifdef H5 
			params = this.getInitParams();
			//#endif

			//#ifdef MP-WEIXIN 
			params = {
				partner: uni.getAccountInfoSync().miniProgram.appId,
				channelNo: "wxxcx",
			}
			code = await self.getCode();
			//#endif
			
			let pdata = {
				reqCode: "8001",
				opencode: code,
				...params
			}
			let defaultData = {
				partner: params.partner,
				channelNo:params.channelNo
			}
			let data = await self.request(pdata);
			
			defaultData.partnerName = data.partnerName;
			defaultData.token = data.token;
			await util.setStorage('defaultData', defaultData);
			
			let storageData = {
				viewType: data.viewType,
				data: data.data
			}
			let tag = util.showType(data.viewType);
			getApp().globalData.tag = tag;
			
			await util.setStorage('config',storageData);
			resolve(storageData)
		})

	},
	//获取用户信息
	getUserInfo(params) {
		let self = this;
		return new Promise(async (resolve, reject) => {
			let pdata = {
				reqCode: "8002",
				...params
			}
			let data = await self.request(pdata,true,'正在校验');
			resolve(data);
		})
	},

	//用户确认注册
	async sureRegister(data) {
		let params = {
			reqCode: "8010",
			userKindId: data.userKind.userKindId,
			uniqueType: data.data.uniqueType,
			uniqueId: data.data.uniqueId,
			reqUrlCode: data.data.reqUrlCode,
			useType: data.useType,
			useCode: data.useCode
		}
		await this.request(params,true,'正在注册');
		await this.getSystemConfig();
		util.showModal("用户绑定成功",function(){
			uni.navigateBack({
				delta: 3
			});
		})
	},
	//修改用户信息
	async updateUser(params) {
		return new Promise(async (resolve, reject) => {
			let pdata = {
				reqCode: "8009",
				...params
			}
			let res = await this.request(pdata,true,'正在修改');
			resolve(res)
		})
	},
	//解除绑定
	unbind(params) {
		return new Promise(async (resolve, reject) => {
			let pdata = {
				reqCode: "8011",
				...params
			}
			let res = await this.request(pdata,true,'正在解绑');
			resolve(res)
		})
	},
	//获取分页数据
	getDataByPage(reqCode, options,curPageNo, showPageNum = 10) {
		let self = this;
		return new Promise(async (resolve, reject) => {
			let pdata = {
				reqCode: reqCode,
				curPageNo: curPageNo,
				...options
			}
			let data = await self.request(pdata);
			resolve(data.data);
		})
	},


	//按年统计
	getDataByYear(params) {
		let self = this;
		return new Promise(async (resolve, reject) => {
			let data = await self.request(params, true, '正在请求');
			resolve(data);
		})
	},

	//用户交易按年统计
	async getOrderDataByYear(options,queryYear) {
		let pdata = {
			reqCode: '8013',
			queryYear: queryYear,
			...options
		}
		return await this.getDataByYear(pdata);
	},

	//用户抄表按年统计
	async getReadMeterDataByYear(options,queryYear) {
		let pdata = {
			reqCode: '8015',
			queryYear: queryYear,
			...options
		}
		return await this.getDataByYear(pdata)
	},

	//用户上报按年统计
	async getMeterUploadByYear(options,queryYear) {
		let pdata = {
			reqCode: '8017',
			queryYear: queryYear,
			...options
		}
		return await this.getDataByYear(pdata)
	},

	//用户上报按月统计
	async getMeterUploadByMonth(options,queryMonth) {
		let pdata = {
			reqCode: '8018',
			queryMonth: queryMonth,
			...options
		}
		return await this.getDataByYear(pdata);
	},
	//或许富文本信息
	async getPhotoArray() {
		let self = this;
		return new Promise(async (resolve, reject) => {
			let pdata = {
				reqCode: '8021',
			}
			let data = await self.request(pdata);
			resolve(data);
		})
	},
	//获取用户交易明细
	getOrderData(options,curPageNo) {
		return new Promise(async (resolve, reject) => {
			let data = await this.getDataByPage('8012', options,curPageNo);
			resolve(data)
		})
	},

	//获取用户抄表明细
	getReadMeterData(options,curPageNo) {
		return new Promise(async (resolve, reject) => {
			let data = await this.getDataByPage('8014', options,curPageNo);
			resolve(data)
		})
	},
	//获取表数据上传明细
	getMeterUploadData(options,curPageNo) {
		return new Promise(async (resolve, reject) => {
			let data = await this.getDataByPage('8016', options,curPageNo);
			resolve(data)
		})
	},
	//测试mock
	testMock() {
		this.request("/userapi/config").then(res=>{
			console.log(res);
		});

		// return new Promise(async (resolve, reject) => {
		// 	let data = await this.getDataByPage('8016', options,curPageNo);
		// 	resolve(data)
		// })
	}
}

module.exports = api;

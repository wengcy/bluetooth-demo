export default {
	//http://172.16.100.52:24000/cqct/tool
	'post|/api': (req) => {
		let methodCode = JSON.parse(req.body).methodCode;
		if (methodCode == "3001") {
			return {
				resultCode: "0000",
				resultMsg: "",
				data: {
					empId: "000000",
					empName: "admin"
				}
			};
		} else if (methodCode == "3002") {
			return {
				resultCode: "0000",
				resultMsg: "",
				data: {
					addrDes: "张村集乡东明古寺-93-1",
					buyNums: 0,
					clbms: [{
							clbmDes: "CQCT-IC卡-金额表",
							clbmid: "05",
							sfIccard: "1",
							sfJe: "1"
						},
						{
							clbmDes: "CQCT-RF卡-气量表",
							clbmid: "06",
							sfIccard: "1",
							sfJe: "1"
						},
						{
							clbmDes: "民用-IC卡-气量表",
							clbmid: "1",
							sfIccard: "1",
							sfJe: "1"
						},
						{
							clbmDes: "ZJCN-IC卡-金额表",
							clbmid: "TJYM04",
							sfIccard: "1"
						},
					],
					initBds: 0,
					lastBds: 0,
					lastOrderTime: "2019-07-24 11:28:38",
					meterClbm: "ZJCN-IC卡-金额表",
					meterId: "1010011607",
					meterSfje: "1",
					priceMode: "1", //1、固定 2、阶梯
					sfIccard: "1",
					ssJe: 50,
					syje: 39.2,
					syql: 14,
					userName: "左顺平",
					zgJe: 39.2,
					zgql: 14
				}
			};
		} else if (methodCode == "3003") {
			return {
				resultCode: "0000",
				resultMsg: "",
				data: {
					userName: "张三",
					addrDes: "城南家园",
					buyNums: "4",
					zgJe: "11.11",
					meterId: "1234567890"
				}
			};
		} else if (methodCode == "3004") {
			return {
				resultCode: "0000",
				resultMsg: "",
				data: {
					initMoney: "1",
					initAmount: "3",
					orderAmount: "5",
					orderMoney: "10",
					syAmount: "13",
					syMoney: "13",
				}
			}
		}

	},

}

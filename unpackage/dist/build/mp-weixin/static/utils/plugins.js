import vm from "@/main.js"
let plugins = {
	install(Vue) {
		Vue.prototype.$encodeParams = function(json) {
			return encodeURIComponent(JSON.stringify(json))
		}
		Vue.prototype.$decodeParams = function(jsonStr) {
			if (jsonStr !== "") {
				return JSON.parse(decodeURIComponent(jsonStr));
			} else {
				return "";
			}
		}
		Vue.prototype.$showModal = function(content, callback = function() {}, showCancel = false, confirmText = "确定") {
			uni.showModal({
				title: '提示',
				content: content,
				showCancel: showCancel,
				confirmText: confirmText,
				success(res) {
					if (res.confirm) {
						callback();
					}
				}
			});
		}
		Vue.prototype.$showLoading = function(content) {
			uni.showLoading({
				title: content,
				mask: true
			})
		}
		// Vue.prototype.$showModal = function(content, callback = function() {}, showCancel = false, confirmText = "确定") {
		// 	vm.$wshowModal({
		// 		title: '提示',
		// 		content: content,
		// 		showCancel: showCancel,
		// 		confirmText: confirmText,
		// 		success(res) {
		// 			if (res.confirm) {
		// 				callback();
		// 			}
		// 		}
		// 	});
		// }
		
		Vue.prototype.$reLaunch = function(url) {
			uni.reLaunch({
				url: url
			});
		}
		Vue.prototype.$navigateBack = function(delta) {
			uni.navigateBack({
				delta: delta
			})
		}
		Vue.prototype.$navigateTo = function(url) {
			uni.navigateTo({
				url: url
			});
		}
		Vue.prototype.$validatenull = function(val){
			// 特殊判断
			if (val && parseInt(val) === 0) return false;
			if (typeof val === 'boolean') {
				return false;
			}
			if (typeof val === 'number') {
				return false;
			}
			if (val instanceof Array) {
				if (val.length === 0) return true;
			} else if (val instanceof Object) {
				if (JSON.stringify(val) === '{}') return true;
			} else {
				if (
					val === 'null' ||
					val == null ||
					val === 'undefined' ||
					val === undefined ||
					val === ''
				) {
					return true;
				}
				return false;
			}
			return false;
		}
	}
}

export default plugins

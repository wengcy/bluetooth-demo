import {
	validCommand,
	getCardTypeApi,
	getCardDataApi,
	writeCardDataApi
} from "../api/api.js"
import vm from "@/main.js"
import bluetooth from './bluetooth.js';
let globalData = getApp().globalData;
import {
	showModal
} from '../utils/utils.js'

let config = [{
	//cardTypeCommand: '121200000000000000000000001C010C049782',//4442卡读整卡命令
	cardTypeCommand: '13122040570277889900000000001C010C2798FC', //4442卡读整卡命令
	powerup: '13122040570277889900000000001C0108A78363',
	powerDown: '13122040570277889900000000001C01092786E7' //上电指令
}]

let bluetoothTools = {
	//保存卡类型
	cardType: "4442",
	cardCommandsArr: [], //保存圈存返回来的指令
	count: 0,
	ccount: 0, //读卡类型索引下标

	//写卡之前的初始化
	writeCardBeforeInit: async function(funName, cardCommands,pageVm = null) {
		globalData.onValueFunStr = funName; //回调方法
		globalData.cardCommands = cardCommands; //当前请求命令数组
		globalData.returnCommands = []; //当前返回命令数组
		globalData.currentCommandsIndex = 0; //当前执行指令索引
		await bluetooth.writeCard(cardCommands[0],pageVm);
	},
	//循环读卡类型命令，支持多张卡的作用
	sendCardCommand() {
		for (let i = this.ccount; i < config.length; i++) {
			this.writeCardBeforeInit("getCardType", [config[i].cardTypeCommand])
		}
	},
	//卡上电
	onPower() {
		globalData.isWriting = true;
		//判断是否连接设备
		let that = this;
		bluetooth.getConnectedBluetoothDevices(function(devices) {
			//如果当前设备已连接，则直接读卡，否则判断当前是否有其他连接设备，如果有，则提示设备只能连接一个，否则，进行连接操作
			if (devices.length > 0) {
				uni.$emit("isShowLoading", true);
				that.writeCardBeforeInit("onPowerCallback", [config[0].powerup])
			} else {
				vm.$navigateTo("/pages/bluetooth/insertCardTip");
			}
		})

	},

	//卡上电命令回调
	async onPowerCallback(readCardDatas) {
		let params = {
			"readCardDatas": readCardDatas,
			"cardType": this.cardType == null ? '4442' : this.cardType
		}
		let [err, data] = await validCommand(params);
		if (err) return;
		this.sendCardCommand();
	},

	//读卡类型
	getCardType: async function(readCardDatas) {
		let params = {
			"readCardDatas": readCardDatas
		}
		let [err, data] = await getCardTypeApi(params);
		if (err) return;
		//获得读全卡命令和类型
		globalData.readCardType = data.cardType;
		this.cardType = data.cardType;
		let cardCommands = data.cardCommands;
		//执行写卡监听操作
		if (this.cardType != null && this.cardType == "4442") {
			// bluetooth.cardTypeCommands = config[this.ccount].cardTypeCommand;
			this.writeCardBeforeInit("getCardData", cardCommands)
		} else {
			util.showModal(`当前不支持此卡类型`);
		}
	},

	//获取全卡数据
	getCardData: async function(readCardDatas) {
		let params = {
			"readCardDatas": readCardDatas,
			"cardType": this.cardType,
		}
		let [err, data] = await getCardDataApi(params);
		if (err) return;
		globalData.currentcardData = data;
		this.getCardDataCallBackByreadCardType(data)
	},
	//根据当前读卡类型判断执行什么回调
	getCardDataCallBackByreadCardType(data) {
		this.writeCard();
	},
	//直接写卡
	async directWriteCard(nextFun, cardCommand,pageVm) {
		this.writeCardBeforeInit(nextFun, [cardCommand],pageVm);
	},
	
	//写卡
	async writeCardValide(readCardDatas, nextFun, cardCommand, callback = function(data) {
		return true
	}) {
		let params = {
			"readCardDatas": readCardDatas,
			"cardType": this.cardType
		}
		let [err, data] = await validCommand(params);
		if (err) return;
		var flag = callback(data);
		if (flag) {
			this.writeCardBeforeInit(nextFun, [cardCommand]);
		}
	},
	//点击写卡流程
	async writeCard() {
		let currentcardData = globalData.currentcardData;
		let writePage = globalData.writePage;
		let writeCardParams = {};
		if(writePage == "getMeterData") {
			writeCardParams = globalData.oldMeterInfo;
		}else{
			writeCardParams = globalData.normalNewMeterInfo;
		}
		let params = {
			"readCardData": currentcardData.readCardData,
			"protocol": currentcardData.protocol,
			"cardType": globalData.readCardType,
			...writeCardParams
		}
		
		let [err, data] = await writeCardDataApi(params);
		if(err) return;
		globalData.writeCardCardData = data;
		this.cardCommandsArr = data.cardCommands;
		//读取错误次数
		this.writeCardBeforeInit("getErrorTimesCallback", [this.cardCommandsArr[0]]);
	},
	//错误次数
	getErrorTimesCallback(readCardDatas) {
		let that = this;
		that.writeCardValide(readCardDatas, "checkCardPasswordCallback", this.cardCommandsArr[1]);
	},
	//校验卡密码
	checkCardPasswordCallback: function(readCardDatas) {
		this.writeCardValide(readCardDatas, "writeCardDataCallback", this.cardCommandsArr[2]);
	},
	//写卡
	writeCardDataCallback(readCardDatas) {
		var that = this;
		var nextFun = "";
		var nextCommand = "";
		var currentIndex = 0;
		nextFun = "updataCardPasswordCallback";
		nextCommand = this.cardCommandsArr[3];
		currentIndex = 2;
		this.writeCardValide(readCardDatas, nextFun, nextCommand);
	},
	//修改卡密码
	updataCardPasswordCallback: function(readCardDatas) {
		this.writeCardValide(readCardDatas, "offElectricityCallback", config[0].powerDown);
	},
	//下电
	async offElectricityCallback(readCardDatas) {
		globalData.isWriting = false;
		uni.$emit("isShowLoading", false);
		showModal("写卡成功", function() {
			vm.$reLaunch("/pages/serviceMeter/inputOldMeterNo")
		});
	}
}
export default bluetoothTools;
